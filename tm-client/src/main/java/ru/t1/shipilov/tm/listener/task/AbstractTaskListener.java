package ru.t1.shipilov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.shipilov.tm.listener.AbstractListener;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.List;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(
                    index + ". " + task.getName() +
                            " | Id: " + task.getId() +
                            " | Created: " + task.getCreated() +
                            " | Status: " + task.getStatus().getDisplayName() + " |"
            );
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + task.getCreated());
    }

}
