package ru.t1.shipilov.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.api.endpoint.*;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shipilov.tm.exception.system.CommandNotSupportedException;
import ru.t1.shipilov.tm.listener.AbstractListener;
import ru.t1.shipilov.tm.util.SystemUtil;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.shipilov.tm.command";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;


    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void createConsoleEvent(@Nullable final String command) {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> command.equals(m.getName()))
                .findFirst()
                .orElse(null);
        if (listener == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> argument.equals(m.getArgument()))
                .findFirst()
                .orElse(null);
        if (listener == null && argument != null) throw new ArgumentNotSupportedException(argument);
        if (listener == null) throw new ArgumentNotSupportedException();
        publisher.publishEvent(listener.getName());
    }

    private boolean processArgument(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        processArgument(arguments[0]);
        return true;
    }

    private void initCommands() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            createConsoleEvent(command);
            System.out.println("[OK]");
            loggerService.command("Executing command: " + command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) initCommands();
    }

}
