package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.SessionDTO;
import ru.t1.shipilov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDTO session) throws Exception;

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
